<?php session_start(); ?>

<!DOCTYPE html>
<html lang="ee">

<head>
    <title>Just Dance</title>
    <meta charset="UTF-8">
    <meta name="author" content="Matis Lepik">
    <meta name="description" content="Just Dance tantsukursused Eestis">
    <meta name="keywords" content="dance">

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <div class="pagewrap">
        <?php require( "content/header.php"); ?>

        <div class="content meist">
            <div class="person">
                <div class="titlebox">
                    <h2>Sandra Ööbik</h2>
                    <p>Projektijuht</p>
                </div>
                <div class="descriptionbox">
                    <img src="img/team/SandraOobik.jpg">
                    <p>Vivamus ultrices ante ut dui porta euismod. Maecenas ornare arcu ac dapibus molestie. Nam finibus nunc diam, eu euismod nisi posuere quis. Aliquam erat volutpat. Donec semper tincidunt sem, at faucibus tortor auctor vel. Cras at risus malesuada leo egestas dictum. Aenean aliquam efficitur condimentum. Maecenas dapibus risus tincidunt gravida feugiat. Donec placerat nisl eu pellentesque fermentum. Sed eu gravida mauris.</p>
                </div>
            </div>

            <div class="person">
                <div class="titlebox">
                    <h2>Siim Elias</h2>
                    <p>Veebihaldur</p>
                </div>
                <div class="descriptionbox">
                    <img src="img/team/SiimElias.jpg">
                    <p>Sed ac eleifend dui. Donec in auctor lectus, non interdum urna. Nulla a nulla metus. Cras pellentesque, libero quis luctus tempor, mi velit gravida sapien, non scelerisque velit felis non turpis. Aenean lacinia gravida accumsan. Fusce mollis ultricies dui, quis pellentesque est dictum ac. Aliquam erat volutpat.</p>
                </div>
            </div>

            <div class="person">
                <div class="titlebox">
                    <h2>Annika Kask</h2>
                    <p>Assistent</p>
                </div>
                <div class="descriptionbox">
                    <img src="img/team/AnnikaKask.jpg">
                    <p>Sed dapibus dui ac placerat varius. Proin id nisi sapien. Aenean consequat, sem tincidunt dapibus blandit, mi tellus sodales lorem, ac ultrices tortor enim et enim. Cras id facilisis lorem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris quis est metus. Maecenas fermentum tempus mauris, vitae facilisis urna tincidunt et. Vestibulum luctus ac odio vel condimentum. Duis ante justo, tincidunt et pulvinar sed, pellentesque vitae eros. Mauris vitae blandit nisi, eget varius tortor. Donec ut leo convallis, tincidunt ex sed, gravida purus. Mauris cursus eros eu feugiat ultricies. Morbi vitae lacinia dui. Nam congue augue ex, vitae commodo metus vehicula id. Integer vel posuere turpis.</p>
                </div>
            </div>
        </div>

        <?php require( "content/footer.php"); ?>
    </div>
    <?php require( "content/navi.php"); ?>

    <script type="text/javascript" src="javascript/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="javascript/scripts.js"></script>
    <script type="text/javascript" src="javascript/loginhandler.js"></script>
    <script type="text/javascript" src="javascript/registerhandler.js"></script>
</body>

</html>