<!DOCTYPE html>
<html lang="ee">

<head>
    <title>Just Dance</title>
    <meta charset="UTF-8">
    <meta name="author" content="Matis Lepik">
    <meta name="description" content="Just Dance tantsukursused Eestis">
    <meta name="keywords" content="dance">

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <div class="pagewrap">
        <?php require( "content/header.php"); ?>

        <div class="content lookursus">
            <?php session_start(); if (isset($_SESSION[ "username"]) && isset($_SESSION[ "status"]) && $_SESSION[ "status"]=="a" ) { } ?>
            <form>
                <input type="text" name="county" placeholder="Piirkond">
                <input type="text" name="name" placeholder="Nimi">
                <input type="text" name="style" placeholder="Stiil">
                <input type="text" name="location" placeholder="Asukoht">
                <input type="text" name="time" placeholder="Aeg">
                <input type="text" name="price" placeholder="Hind">
                <input type="text" name="description" placeholder="Kirjeldus">
                <div class="button" id="submitcoursebutton">Loo kursus</div>
            </form>
        </div>

        <?php require( "content/footer.php"); ?>
    </div>
    <?php require( "content/navi.php"); ?>

    <script type="text/javascript" src="javascript/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="javascript/scripts.js"></script>
    <script type="text/javascript" src="javascript/loginhandler.js"></script>
    <script type="text/javascript" src="javascript/registerhandler.js"></script>
    <script type="text/javascript" src="javascript/kursushandler.js"></script>
</body>

</html>