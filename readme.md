# Just Dance NoorMeister 2015 #

This was my entry for the Noor Meister 2015 competition. Most of the things in this website were done in 10 hours with no internet connection. We were given a subject, some necessary features and some image assets. I wanted to keep it the way it was after the competition, so I'm not going to finish it.

**You can see the [live version](http://matislepik.eu/justdance/) on my website.**

### Setup ###

This must run in a php server. Run the SQL queries from initdb.txt to set up the database and add some basic values.

### CONTACT ###

- www.matislepik.eu

- E-mail: matis.lepik@gmail.com

- Skype: matis.lepik