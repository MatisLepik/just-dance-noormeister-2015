<?php session_start(); ?>

<!DOCTYPE html>
<html lang="ee">

<head>
    <title>Just Dance</title>
    <meta charset="UTF-8">
    <meta name="author" content="Matis Lepik">
    <meta name="description" content="Just Dance tantsukursused Eestis">
    <meta name="keywords" content="dance">

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <div class="pagewrap">
        <?php require( "content/header.php"); ?>
        <div class="content loginpage">
            <div class="registerbox">
                <p class="resultmsg">Hello</p>
                <form>
                    <input type="text" placeholder="Kasutajanimi (e-posti aadress)" name="username" />
                    <input type="password" placeholder="Parool" name="password" />
                    <input type="password" placeholder="Parooli kontroll" name="password2" />
                    <input type="text" placeholder="Sünnipäev" name="birthday" />
                    <label>
                        <input type="checkbox" name="agree" />
                        <span>Nõustud reeglitega?</span>
                    </label>
                    <br>
                    <label>
                        <input type="checkbox" name="newsletter" />
                        <span>Tahad liituda uudiskirjaga?</span>
                    </label>
                    <div class="registerbutton button" id="registerbutton">Registreeri</div>
                </form>
            </div>
            <div class="loginbox">
                <p class="resultmsg">Hello</p>
                <form>
                    <input type="text" placeholder="Username" name="username" />
                    <input type="password" placeholder="Password" name="password" />
                    <div class="loginbutton button" id="loginbutton">Logi sisse</div>
                </form>
            </div>
        </div>
        <?php require( "content/footer.php"); ?>
    </div>
    <?php require( "content/navi.php"); ?>

    <script type="text/javascript" src="javascript/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="javascript/scripts.js"></script>
    <script type="text/javascript" src="javascript/loginhandler.js"></script>
    <script type="text/javascript" src="javascript/registerhandler.js"></script>
</body>

</html>