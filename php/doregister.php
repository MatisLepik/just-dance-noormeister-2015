<?php
	session_start();

	require_once('globals.php');

	//Fetch form data
	$form_username = $_POST['username'];
	$form_password = $_POST['password'];
	$form_birthday = $_POST['birthday'];
	$form_newsletter = $_POST['newsletter'];

	//Normalize newsletter to sql datatype
	if ($form_newsletter == "false") {
		$form_newsletter = 0;
	} else {
		$form_newsletter = 1;
	}

	//Check if username already exists
	try {
		$sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
		$query = $sql -> prepare("SELECT username FROM users WHERE username=:u");
		$query -> bindParam(':u', $form_username);
		$query -> execute();
		$users = $query -> fetchAll(PDO::FETCH_ASSOC);
		if (!empty($users)) {
			echo "Username already exists.";
		} else {
			//Light check to make sure email is valid
	if (preg_match("/@/i", $form_username) && preg_match("/\./i", $form_username)) {
		//Send to SQL
		try {
            $sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
			$query = $sql -> prepare("INSERT INTO users (username, password, birthday, newsletter) VALUES (:u, :p, :b, :n)");
			$query -> bindParam(':u', $form_username);
			$query -> bindParam(':p', password_hash($form_password, PASSWORD_BCRYPT));
			$query -> bindParam(':b', $form_birthday);
			$query -> bindParam(':n', $form_newsletter);
			$query -> execute();
			if ($query->errorCode() == "00000") {
				echo "true";
			} else {
				echo "Error in creating a user!";
			}
		} catch (PDOException $e) {
			echo $e;
		}
	} else {
		echo "Invalid e-mail address!";
	}
		}
	} catch (PDOException $e) {
		echo $e;
	}
?>