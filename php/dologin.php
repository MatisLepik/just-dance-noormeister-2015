<?php
	session_start();

	require_once('globals.php');
	
	// Fetch post data into variables
	$form_username = $_POST['username'];
	$form_password = $_POST['password'];

	//Create database connection
	try {
		$sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
		$query = $sql -> prepare("SELECT username, password, status FROM users WHERE username=:u");
		$query -> bindParam(':u', $form_username);
		$query -> execute();
		$users = $query -> fetchAll(PDO::FETCH_ASSOC);

		//Check if there was an error
		if (!empty($users)) {
			if ($query->errorCode() == "00000") {
				if (password_verify($form_password, $users[0]['password'])) {
					$_SESSION['username'] = $users[0]['username'];
					$_SESSION['status'] = $users[0]['status'];
					echo "true";
				} else {
					echo "Vale parool.";
				}
			} else {
				echo "Login ebaõnnestus.";
			}
		} else {
			echo "Sellist kasutajat ei ole!";
		}
	} catch (PDOException $e) {
		echo "Serveriga tekkis probleem. Vabandust ebamugavuste pärast!";
	}
?>