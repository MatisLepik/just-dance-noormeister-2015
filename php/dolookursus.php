<?php
	session_start();
	require_once('globals.php');
	//Fetch form data
	$form_name = $_POST['name'];
	$form_style = $_POST['style'];
	$form_county = $_POST['county'];
	$form_time = $_POST['time'];
	$form_price = $_POST['price'];
	$form_location = $_POST['location'];
	$form_description = $_POST['description'];

	//Send to SQL
	try {
		$sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
		$query = $sql -> prepare("INSERT INTO courses (name, style, county, time, price, location, description) VALUES (:name, :style, :county, :time, :price, :location, :description)");
		$query -> bindParam(':name', $form_name);
		$query -> bindParam(':style', $form_style);
		$query -> bindParam(':county', $form_county);
		$query -> bindParam(':time', $form_time);
		$query -> bindParam(':price', $form_price);
		$query -> bindParam(':location', $form_location);
		$query -> bindParam(':description', $form_description);
		$query -> execute();
		if ($query->errorCode() == "00000") {
			echo "true";
		} else {
			echo json_encode($query->errorInfo());
		}
	} catch (PDOException $e) {
		echo $e;
	}
?>