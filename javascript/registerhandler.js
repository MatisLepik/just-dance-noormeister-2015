$('#registerbutton').on('click', function() {

	//Fetch login data from input fields
	var username = $('.registerbox input[name="username"]')[0].value;
	var password = $('.registerbox input[name="password"]')[0].value;
	var password2 = $('.registerbox input[name="password2"]')[0].value;
	var birthday = $('.registerbox input[name="birthday"]')[0].value;
	var agree = $('.registerbox input[name="agree"]')[0].checked;
	var newsletter = $('.registerbox input[name="newsletter"]')[0].checked;

	tryRegister(username, password, password2, birthday, agree, newsletter);
});

function tryRegister(username, password, password2, birthday, agree, newsletter) {

	// Validates form
	if (agree) {
		if (username != "" && password != "" && password2 != "" && birthday != "") {
			if (password === password2) {
				if (password.length >= 6) {
					sendRegistration(username, password, birthday, newsletter);
				} else {
					resultMsg("Parool peab olema vähemalt 6 sümbolit!", "#e22a40");
				}
			} else {
				resultMsg("Paroolid ei ole võrdsed!", "#e22a40");
			}
		} else {
		resultMsg("Palun täitke kõik väljad!", "#e22a40");
		}
	} else {
		resultMsg("Registreerimiseks peate nõustuma reeglitega!", "#e22a40");
	}
}

function sendRegistration(username, password, birthday, newsletter) {

	//Sends registration data to php
	$.post("php/doregister.php", {
		username: username,
		password: password,
		birthday: birthday,
		newsletter: newsletter,
	}, function(data) {
		if (data != "true") {
			resultMsg(data, "#e22a40");
		} else {
			resultMsg("Registration complete!", "#227ed3");
		}
	});
}

function tryLogout() {
	$.post("php/dologout.php", function() {
 		location.reload();
 	});
}

function resultMsg(msg, color) {
	//If login fails, clear input fields
	$('.registerbox .resultmsg').css('color',color).html(msg).slideDown(200);
	//..and play an animation
}