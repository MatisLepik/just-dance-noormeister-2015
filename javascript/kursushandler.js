$('#submitcoursebutton').on('click', function() {
	//Fetch login data from input fields
	var name = $('.lookursus input[name="name"]')[0].value;
	var style = $('.lookursus input[name="style"]')[0].value;
	var county = $('.lookursus input[name="county"]')[0].value;
	var time = $('.lookursus input[name="time"]')[0].value;
	var price = $('.lookursus input[name="price"]')[0].value;
	var location = $('.lookursus input[name="location"]')[0].value;
	var description = $('.lookursus input[name="description"]')[0].value;
    
    if (name == "" || style == "" || county== "" || time == "" || price == "" || location == "" || description == "") {
        alert("Please fill in all the forms");
        return;
    } 

	$.post("php/dolookursus.php", {
		name:name,
		style:style,
		county:county,
		time:time,
		price:price,
		location:location,
		description:description
	}, function(data) {
		if (data != "true") {
			alert("Kursuse lisamisega tekkis probleem!");
			console.log(data);
		} else {
			window.history.back();
		}
	});	
});