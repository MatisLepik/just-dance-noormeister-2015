/* Event listeners */
$('.piirkondpage .course').on('click', function(evt) {
	expandCourse(evt.currentTarget);
});

$('.piirkondpage .extrabar a').on('click', function(evt) {
	evt.stopPropagation();
});

/* Expands a course under Piirkond by showing the extra information (description, etc)  */
function expandCourse(course) {
	for (var i = 0; i < course.children.length; i++) {
		if ($(course.children[i]).hasClass("extrabar")) {
			$(course.children[i]).toggle(100);
		}
	}
}