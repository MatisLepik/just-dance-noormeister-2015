$('#loginbutton').on('click', function() {

	//Fetch login data from input fields
	var username = $('.loginbox input[name="username"]')[0].value;
	var password = $('.loginbox input[name="password"]')[0].value;

	tryLogin(username, password);
});

 $('#logoutbutton').on('click', tryLogout);

function tryLogin(username, password) {

	$.post("php/dologin.php", {
		username: username,
		password: password
	}, function(data) {
		if (data != "true") {
			resultMsg(data);
		} else {
			window.location.href = "index.php";
		}
	});
}

function tryLogout() {
	$.post("php/dologout.php", function() {
 		location.reload();
 	});
}

function resultMsg(msg) {
	//If login fails, clear input fields
	$('.loginbox .resultmsg').html(msg).slideDown(200);
	//..and play an animation
}