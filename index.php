<?php session_start(); ?>

<!DOCTYPE html>
<html lang="ee">

<head>
    <title>Just Dance</title>
    <meta charset="UTF-8">
    <meta name="author" content="Matis Lepik">
    <meta name="description" content="Just Dance tantsukursused Eestis">
    <meta name="keywords" content="dance">

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <div class="pagewrap">

        <?php require("content/header.php"); ?>

        <div class="content avaleht">
            <div class="quotecontainer">
                <h1 class="quote">“Dance is the hidden language of the soul” -Marta Graham</h1>
            </div>
            <div class="descriptioncontainer">
                <div class="descriptiontext">
                    <p>Kunagi pole liiga hilja alustada. Just Dance veebilehelt leiad tantsukursusi üle Eesti.</p>
                    <p>Sulle sobiva kursuse leidmine on imelihtne - vali piirkond või tantsustiil ning sulle avaneb loetelu kursustest.</p>
                    <p>Ka tantsukaaslaste leidmine on imelihtne. Kirjuta sulle sobiva kursuse kommentaaridesse oma kaaslaseotsingust ning jää ootama vastust. Ehk leiad juba homme oma unelmate partneri? Kursuste kommenteerimiseks registreeru “Just Dance“ kasutajaks.</p>
                </div>
                <img src="img/silhouette.png" width="200" height="200" alt="Silhouette dancers">
            </div>
        </div>
        <div class="gallerycontainer">
            <div class="gallery">
                <div class="piirkonnad">
                    <h1>Piirkonnad</h1>
                    <div class="galleryitem pic harjumaa"><a href="piirkond.php?piirkond=Harjumaa">Harjumaa</a>
                    </div>
                    <div class="galleryitem pic saaremaa"><a href="piirkond.php?piirkond=Saaremaa">Saaremaa</a>
                    </div>
                    <div class="galleryitem pic tartumaa"><a href="piirkond.php?piirkond=Tartumaa">Tartumaa</a>
                    </div>
                    <div class="galleryitem pic parnumaa"><a href="piirkond.php?piirkond=Pärnumaa">Pärnumaa</a>
                    </div>
                    <div class="galleryitem pic idavirumaa"><a href="piirkond.php?piirkond=Ida-Virumaa">Ida-Virumaa</a>
                    </div>
                </div>
                <div class="stiilid">
                    <h1>Stiilid</h1>
                    <div class="galleryitem pic flamenko"><a href="#">Flamenko</a>
                    </div>
                    <div class="galleryitem pic hiphop"><a href="#">Hip-hop</a>
                    </div>
                    <div class="galleryitem pic seltskonnatants"><a href="#">Seltskonnatants</a>
                    </div>
                    <div class="galleryitem pic rahvatants"><a href="#">Rahvatants</a>
                    </div>
                    <div class="galleryitem pic nabatants"><a href="#">Nabatants</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require( "content/navi.php"); ?>
    <?php require( "content/footer.php"); ?>

    <script type="text/javascript" src="javascript/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="javascript/scripts.js"></script>
    <script type="text/javascript" src="javascript/loginhandler.js"></script>
    <script type="text/javascript" src="javascript/registerhandler.js"></script>
</body>

</html>