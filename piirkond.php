<?php session_start(); ?>

<!DOCTYPE html>
<html lang="ee">

<head>

    <title>Just Dance</title>
    <meta charset="UTF-8">
    <meta name="author" content="Matis Lepik">
    <meta name="description" content="Just Dance tantsukursused Eestis">
    <meta name="keywords" content="dance">

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <div class="pagewrap">
        <!-- Header, footer, navi get called in via php so they only have to be styled once -->
        <?php require( "content/header.php"); ?>

        <div class="content piirkondpage noselect">
            <div class="titlebar">
                <h1 id="piirkondname"><?php echo $_GET['piirkond']; ?></h1>
                <label>
                    <input type="text" name="filter">
                    <span>Filtreeri stiili järgi: </span>
                </label>
                <?php if (isset($_SESSION[ "username"]) && isset($_SESSION[ "status"]) && $_SESSION[ "status"]=="a" ) { echo '<a href="lookursus.php">Loo kursus</a>'; } ?>
            </div>

            <div class="courses">
                <?php
						require_once('php/globals.php');
						try {
                            $sql = new PDO("mysql:host=".DB_SERVERNAME.";dbname=".DB_DBNAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
							$query = $sql -> prepare("SELECT * FROM courses WHERE county=:county");
							$query -> bindParam(':county', $_GET['piirkond']);
							$query -> execute();
							$courses = $query -> fetchAll(PDO::FETCH_ASSOC);
							//Check if there was an error
							if ($query->errorCode() == "00000") {
								if (empty($courses)) {
									echo "<p style='text-align:center;'>Siin piirkonnas ei ole veel ühtegi kursust.</p>";
								}
								foreach ($courses as $course) {
									echo '<div class="course">
											<div class="titlebar">
												<h2>'.$course['name'].'</h2>
												<span>'.$course['style'].'</span>
											</div>
											<div class="infobar">
												<ul>
													<li>Asukoht: '.$course['location'].'</li>
													<li>Toimub: '.$course['time'].'</li>
													<li>Hind: '.$course['price'].'€</li>
												</ul>
											</div>
											<div class="extrabar">
												<p>'.$course['description'].'</p>
												<div class="extralinks">
													<a href="#">Lisainformatsioon</a>
													<a href="#">Kommentaarid</a>
												</div>
											</div>
										</div>';
								}
							} else {
								echo "Serveriga tekkis probleem. Vabandust ebamugavuste pärast!";
							}
						} catch (PDOException $e) {
							echo "Serveriga tekkis probleem. Vabandust ebamugavuste pärast!";
						}
					?>
            </div>
        </div>

        <?php require( "content/footer.php"); ?>
    </div>
    <?php require( "content/navi.php"); ?>

    <script type="text/javascript" src="javascript/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="javascript/scripts.js"></script>
    <script type="text/javascript" src="javascript/loginhandler.js"></script>
    <script type="text/javascript" src="javascript/registerhandler.js"></script>
</body>

</html>